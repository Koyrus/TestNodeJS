var express  = require('express');
var cors = require('cors')
var app = express();
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var db;
var ObjectID  = require('mongodb').ObjectID;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});
var movies = [];
app.get('/' ,function (req,res) {
    res.send('HelloApi');
});
app.get('/movies' , function (req,res) {
    db.collection('movies').find().toArray(function (err  , docs) {
        if (err) {
            console.log(err)
            res.sendStatus(500)
        }
        res.send(docs)
    })
});
app.get('/movies/:id' , function (req,res) {
            db.collection('movies').findOne({_id : ObjectID(req.params.id)} , function (err, doc) {
                if (err) {
                    console.log(err);
                   return res.sendStatus(500);
                }
                res.send(doc)
                res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            })
});
app.post('/movies' ,function (req,res) {
    var movie = {
        name : req.body.name,
        description : req.body.description,
        rating : Number(req.body.rating),
        released : Number(req.body.released)

    };
    db.collection('movies').insert(movie , function (err , result) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
    });
    res.sendStatus(200)
});
app.put('/movies/:id' , function (req,res) {
    db.collection('movies').updateMany(
        {_id : ObjectID(req.params.id) },
        {name : req.body.name},
         {description :req.body.description},
         {rating : Number(req.body.rating)},
         {released :Number(req.body.released)},
        function (err , result) {
                if (err){
                    console.log(err);
                     return res.sendStatus(500);
                }
                res.sendStatus(200)
                console.log(res);
        }
    );
});
app.delete('/movies/:id' , function (req,res) {
         db.collection('movies').deleteOne(
             {_id : ObjectID(req.params.id)},
             function (err, result) {
                 if (err){
                     console.log(err)
                     return res.sendStatus(500)
                 }
                 res.sendStatus(200)
             }
         )
});
MongoClient.connect('mongodb://localhost:27017/api' , function (err ,  database) {
    if (err) {
        return console.log(err);
    }
    db = database;
    app.listen(3333 , function () {
        console.log('Api working on 3333')
    })
});